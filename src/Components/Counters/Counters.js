import React from 'react';
import PropTypes from 'prop-types';
import Counter from '../Counter';

const Counters = ({ counters, onIncrement, onDecrement }) => {
  return counters.map((counter, idx) => (
    <Counter
      key={idx}
      value={counter}
      onIncrement={() => onIncrement(idx)}
      onDecrement={() => onDecrement(idx)}
    />
  ));
};

Counters.propTypes = {
  counters: PropTypes.arrayOf(PropTypes.number).isRequired,
  onIncrement: PropTypes.func.isRequired,
  onDecrement: PropTypes.func.isRequired,
};

export default Counters;
