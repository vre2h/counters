import React from 'react';
import CountersContainer from '../../Containers/CountersContainer';
import CouterMakerContainer from '../../Containers/CounterMakerContainer';

const Main = () => {
  return (
    <div className="App">
      <CountersContainer />
      <CouterMakerContainer />
    </div>
  );
};

export default Main;
