import React from 'react';
import PropTypes from 'prop-types';

const CounterMaker = ({ onAddCounter, onRemoveCounter }) => {
  return (
    <div className="actions">
      <button onClick={onAddCounter}>Add Counter</button>
      <button onClick={onRemoveCounter}>Remove Counter</button>
    </div>
  );
};

CounterMaker.propTypes = {
  onAddCounter: PropTypes.func.isRequired,
  onRemoveCounter: PropTypes.func.isRequired,
};

export default CounterMaker;
