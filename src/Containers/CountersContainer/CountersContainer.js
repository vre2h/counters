import { connect } from 'react-redux';
import Counters from '../../Components/Counters';

const mapStateToProps = state => ({
  counters: state.counters,
});

const mapDispatchToProps = dispatch => ({
  onIncrement: id => dispatch({ type: 'INCREMENT', id }),
  onDecrement: id => dispatch({ type: 'DECREMENT', id }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Counters);
