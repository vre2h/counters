import { connect } from 'react-redux';
import CounterMaker from '../../Components/CounterMaker';

const mapStateToProps = state => ({
  counters: state.counters,
});

const mapDispatchToProps = dispatch => ({
  onAddCounter: id => dispatch({ type: 'ADD_COUNTER', id }),
  onRemoveCounter: id => dispatch({ type: 'REMOVE_COUNTER', id }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CounterMaker);
