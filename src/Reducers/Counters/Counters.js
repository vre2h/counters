const inc = (counters, id) => [
  ...counters.slice(0, id),
  counters[id] + 1,
  ...counters.slice(id + 1),
];

const dec = (counters, id) => [
  ...counters.slice(0, id),
  counters[id] - 1,
  ...counters.slice(id + 1),
];

export default (state = [0], action) => {
  switch (action.type) {
    case 'INCREMENT':
      return inc(state, action.id);
    case 'DECREMENT':
      return dec(state, action.id);
    case 'ADD_COUNTER':
      return state.concat([0]);
    case 'REMOVE_COUNTER':
      return state.slice(0, state.length - 1);
    default:
      return state;
  }
};
